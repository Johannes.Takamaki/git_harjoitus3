const expect = require('chai').expect;
const assert = require('chai').assert;
const { Lause } = require('../src/mylib');
const mylib = require('../src/mylib');



before(() => {
    console.log('Testiä edeltävä viesti')

})

describe('Unit testing mylib.js', () => {



it('Should return 16 when using function with a=1, b=3, c=5', () => {
const result = mylib.sum(1,3,5); // 1 + 3 * 5
expect(result).to.equal(16); // result expected to equal 16
})

it('Assert luk1 is not equal luk2', () => {
    assert(luk1 !== luk2)
})

it('Should return a string with sentence "Minkä takia" ', () => {
const tulos = mylib.Lause('Minkä ', 'takia');

expect(tulos).to.equal("Minkä takia"); // result expected to be sentence Minkä takia
})

after(() => {
    console.log('Testin jälkeinen viesti')

})

});